from PIL import Image


def rgb_of_pixel(img_path, x, y):
    im = Image.open(img_path).convert('RGB')
    r, g, b = im.getpixel((x, y))
    a = (r, g, b)
    return a


def main():
	img = "go.png"
	for y in range (0,80):
		for x in range (0,80):
			n = rgb_of_pixel(img, x, y)
			print(n," ",end="")
			#print (rgb_of_pixel(img, x,y))
			rgb_of_pixel(img, x,y)
			
main()

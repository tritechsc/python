# python
Clone the project (repository = repo):
```
git clone https://gitlab.com/tritechsc/python.git
```
Online Python Interpreter:
```
https://trinket.io/
```
'''
https://trinket.io/
'''
```
https://www.w3schools.com/python/
```

https://www.w3schools.com/python/

Color pickers:

https://colorpicker.me/

https://www.w3schools.com/colors/colors_picker.asp


# Install Python

Install Python3 on Windows 10 (Video by Craig Coleman)

https://www.youtube.com/watch?v=6YmkqjehvfQ

Install Python on Linux

Python is native on most Linux systems.
Additional packages need to be installed run some scripts such and turtle graphics.
Type in a Linux terminal:
```
sudo apt install python3-pip 
pip install Pillow 
pip3 install pyserial
```

# History of Python

Python was created in the early 1990s by Guido van Rossum at Stichting Mathematisch Centrum 

```
https://docs.python.org/3/license.html
```
'''
https://docs.python.org/3/license.html
'''

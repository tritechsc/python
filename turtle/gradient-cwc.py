import turtle
import math

def hexcon(num):
	key = "0123456789abcdef" # hex key
	h = ""
	h16 = int(num/16)
	h1 = num % 16
	h = key[h16]+ key[h1]
	return h

def unit_circle(t):
	for n in range (0,375,15):
		rad = (3.141592 / 180) * n
		x = math.cos(rad) * 101
		y = math.sin(rad) * 101
		greenint = int(rad*100)
		if (greenint > 255):
			greenint = 255
		greenHex = hexcon(greenint)
		hexcolor = "#"+"00"+greenHex+"00"
		t.pencolor(hexcolor)
		t.penup()
		t.goto(x,y)
		t.pendown()
		t.circle(10)

def grad(t,red, green, blue):
	thecolor = "#ff0000"
	x = 0
	while(x < 101):
		redHex = hexcon(red); greenHex = hexcon(green); blueHex = hexcon(blue)
		thecolor = "#"+redHex+greenHex+blueHex
		t.color(thecolor)
		print (x,red,green, blue, thecolor)
		t.goto(x,10)
		x = x + 3
		t.goto(x,-10)
		red = 0
		green = green + 10
		blue = 0
		if(red > 255):
			red = 0
		if(green > 255):
			green = 0
		if(blue > 255):
			blue = 0
		if(red > 255):
			red = 0
		if(green > 255):
			green = 0
		if(blue > 255):
			blue = 0

	
def main():
	w = turtle.Screen()
	w.clear()
	t = turtle.Turtle()
	t.speed(0)
	t.hideturtle()
	#t.penup()
	red = 255;green = 0; blue = 0
	grad(t,red, green, blue)
	unit_circle(t)
	w.exitonclick()	
		
if __name__ == "__main__":
	main()



